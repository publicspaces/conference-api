module.exports = ({ env }) => ({

  email: {
    config: {
      provider: 'sendmail',
      settings: {
        defaultFrom: 'team@publicspaces.net',
        defaultReplyTo: 'team@publicspaces.net',
      },
    },
  },
  //
  // 

  upload: {
    breakpoints: {
      xlarge: 1920,
      large: 1000,
      medium: 640,
      small: 500,
      xsmall: 64
    }
  },
  ///
  deepl: {
    enabled: true,
    config: {
      // your DeepL API key
      apiKey: '85c00e83-b15a-76d9-5fc9-c7c99d2aea9f',
      // whether to use the free or paid api, default true
      freeApi: false,
      // Which field types are translated (default string, text, richtext, components and dynamiczones)
      translatedFieldTypes: [
        'string',
        'text',
        'richtext',
        'component',
        'dynamiczone',
      ],
      // If relations should be translated (default true)
      translateRelations: false,
      // You can define a custom glossary to be used here (see https://www.deepl.com/docs-api/managing-glossaries/)
      //glossaryId: 'customGlossary',
    },
  },
  ///
  "rest-cache": {
    enabled: true,
    config: {
      provider: {
        name: "memory",
        options: {
          max: 3276700,
          maxAge: 60,
        },
      },
      strategy: {
        enableEtag: true,
        enableXCacheHeaders: true,
        resetOnStartup: true,
        hitpass: false,
        contentTypes: [
          // list of Content-Types UID to cache
          "api::event.event",
          "api::location.location",
          "api::track.track",
          "api::page.page",
          "api::person.person",
          "api::newsfeed.newsfeed",
          "api::homepage.homepage",
        ],
      },
    },
  },
});