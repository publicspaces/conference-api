module.exports = {
  routes: [
    {
     method: 'GET',
     path: '/ical',
     handler: 'ical.getAll',
     config: {
       policies: [],
       middlewares: [],
     },
    }
  ],
};
