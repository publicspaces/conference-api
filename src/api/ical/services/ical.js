'use strict';

const ical = require('ical-generator');
const moment = require('moment');
/**
 * ical service.
 */


module.exports = {
  getEvents: async (track = null, date = null,  locale = 'nl') => {
    try {
      // fetching data

      const filters = {
        type: { $not: 'break' }
      };
      if (track) {
        filters.tracks = {
            slug: track
        }
      }
      if (date) {
        const s = moment(date).startOf("day");
        const e = moment(date).endOf("day");
        filters.start = {
                $lt: e.toDate(),
                $gt: s.toDate()
        }
      }

      const entries = await strapi.entityService.findMany(
        "api::event.event",
        {
          fields: ["id", "name","headline","start","end","slug", "createdAt"],
          publicationState: 'live',
          locale: locale,
          populate: {
            location: {
                fields: ["name"]
            },
            tracks: {
                fields: ["name","slug"]
            }
          },
          filters: filters
        }
      );

      // add itemt to ical feed
      const icalFeed = ical({prodId:'//publicspaces.net//ical-generator//EN',});
      if (entries && Array.isArray(entries)) {
        for (let item of entries) {
            const start = new Date(item.start);
            let end = null;
            if (item.end) {
             end = new Date(item.start);
             end.setHours(parseInt(item.end.substr(0,2)), parseInt(item.end.substr(3,2)));
            }
            const event = icalFeed.createEvent({
                id: item.id,
                start: start,
                end: end || start,
                timezone: 'Europe/Amsterdam',
                summary: item.name || '',
                description: item.headline || '',
                organizer: 'PublicSpaces <info@publicspaces.net>',
                url: `https://conference.publicspaces.net/session/${item.slug}`
            });
            if (item.location) {
                event.location({
                    title: item.location.name
                })
            }
        }
      }

      // return the reduced data
      return icalFeed.toString();
    } catch (err) {
      return err;
    }
  },
};