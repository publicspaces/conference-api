'use strict';

/**
 * A set of functions called "actions" for `ical`
 */

module.exports = {
  getAll: async (ctx, next) => {
    try {
          const data = await strapi
            .service("api::ical.ical")
            .getEvents(ctx.request.query.track, ctx.request.query.date, ctx.request.query.locale);    
          ctx.set('Content-Type', 'text/calendar') 
          ctx.body = data;
        } catch (err) {
          ctx.badRequest("Post report controller error", { moreDetails: err });
        }
  }
};
