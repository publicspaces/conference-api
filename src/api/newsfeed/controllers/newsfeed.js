'use strict';

const axios = require('axios');

/**
 *  newsfeed controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::newsfeed.newsfeed',({ strapi }) => ({
  async findAll(ctx) {
    
    const entries = await strapi.db.query("api::newsfeed.newsfeed").findMany();
    const endpoint = `${entries[0].mastodon_server}/api/v1/accounts/${entries[0].account_id}/statuses`
    const { data } = await axios.get(endpoint);

    ctx.body = data;
  },
}));
